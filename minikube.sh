#!/bin/bash

###MINIKUBE installation for Ubuntu

sudo apt update -y
sudo apt upgrade -y
sudo apt install -y curl wget apt-transport-https
wget https://github.com/kubernetes/minikube/releases/download/v1.25.2/minikube-linux-amd64
sudo cp minikube-linux-amd64 /usr/local/bin/minikube
sudo chmod +x /usr/local/bin/minikube
minikube version
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin/
kubectl version -o yaml
sudo usermod -aG docker $USER && newgrp docker   ###adding user to docker group
minikube config set driver docker
minikube start --driver=docker



#https://minikube.sigs.k8s.io/docs/start/   #official document