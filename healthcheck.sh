#!/bin/bash

status=$(kubectl get pods -n fastapi-ns | awk '{print $3}')
echo $status
if [[  $status =~ Running ]];
then
  echo "Your deployment is healthy"
  true
  exit 0
else
  echo "Check your deployment"
  false
  exit 1
fi
