

![Getting Started](Capture.PNG)




GITLAB-RUNNER REGISTER

curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb
sudo gitlab-runner register



https://gitlab.com/
Settings > CI/CD > Runners > Token


#https://docs.gitlab.com/runner/install/linux-manually.html   #official document



###K8S Cluster Sync Gitlab###

Agent Access Token : <CheckYourOwn>
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install minikube gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.1.0 \
    --set config.token=<CheckYourOwn> \
    --set config.kasAddress=wss://kas.gitlab.com



###HARBOR with HELM  (Optional replaced with Gitlab-Registry)###

sudo curl -L https://mirror.openshift.com/pub/openshift-v4/clients/helm/latest/helm-linux-amd64 -o /usr/local/bin/helm
sudo chmod +x /usr/local/bin/helm
helm version
helm repo add harbor https://helm.goharbor.io
helm repo update
wget https://raw.githubusercontent.com/goharbor/harbor-helm/master/values.yaml
nano values.yaml
#helm upgrade harbor harbor/harbor -f values.yml -n harbor   ###to update already existing values.yaml
helm install harbor harbor/harbor -f values.yaml -n harbor
helm status harbor